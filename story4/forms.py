from django import forms
from .models import MataKuliah

class CreateMatkul(forms.Form):
    attr = {
        'type': 'text',
        'class': 'form-control',
    }

    nama_matkul = forms.CharField(label='Nama Mata Kuliah', max_length=50,  required=True, widget=forms.TextInput(attrs=attr))
    nama_dosen = forms.CharField(label='Nama Dosen', max_length=50,  required=True, widget=forms.TextInput(attrs=attr))
    jumlah_sks = forms.CharField(label='Jumlah SKS', max_length=50,  required=True, widget=forms.TextInput(attrs=attr))
    deskripsi_matkul = forms.CharField(label='Deskripsi', max_length=50,  required=True, widget=forms.TextInput(attrs=attr))
    semester = forms.CharField(label='Semester', max_length=50,  required=True, widget=forms.TextInput(attrs=attr))
    ruang_kelas = forms.CharField(label='Ruang Kelas', max_length=50,  required=True, widget=forms.TextInput(attrs=attr))

class CreateKegiatan(forms.Form):
    attr = {
        'type': 'text',
        'class': 'form-control',
    }

    nama_kegiatan = forms.CharField(label='Nama Kegiatan', max_length=50, required=True, widget=forms.TextInput(attrs=attr))

class CreatePeserta(forms.Form):
    attr = {
        'type': 'text',
        'class': 'form-control',
    }

    nama_peserta = forms.CharField(label='Nama Peserta', max_length=50, required=True, widget=forms.TextInput(attrs=attr))

class SearchForm(forms.Form):
	attr = {
	    'type' : 'query',
	    'class' : 'form-control',
	    'placeholder' : '...',
	    'id' : 'search-name'
	}

	nama_buku = forms.CharField(max_length=300, label="Masukkan kata pencari", required=False, widget=forms.TextInput(attrs=attr))