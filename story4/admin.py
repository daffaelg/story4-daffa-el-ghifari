from django.contrib import admin
from .models import MataKuliah, Kegiatan, Peserta

admin.site.register(MataKuliah)
admin.site.register(Kegiatan)
admin.site.register(Peserta)
