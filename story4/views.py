from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect, request
from .forms import CreateMatkul, CreateKegiatan, CreatePeserta, SearchForm
from .models import MataKuliah, Kegiatan, Peserta, KegiatanTemp

def profile(request):
    return render (request, 'profile.html')

def desc(request):
    return render (request, 'desc.html')

def story1(request):
    return render (request, 'story1.html')

def AddMatkul(request):
    response = {'create_matkul' : CreateMatkul}
    return render (request, 'addmatkul.html', response)

def SaveMatkul(request):
    response = {'save_status':''}
    if (request.method == 'POST'):
        form = CreateMatkul(request.POST)
        if (form.is_valid()):
            nama_matkul = form.cleaned_data['nama_matkul'] 
            nama_dosen = form.cleaned_data['nama_dosen'] 
            jumlah_sks = form.cleaned_data['jumlah_sks']
            deskripsi_matkul = form.cleaned_data['deskripsi_matkul'] 
            semester = form.cleaned_data['semester'] 
            ruang_kelas = form.cleaned_data['ruang_kelas']

            matkul = MataKuliah(
                nama_matkul = nama_matkul,
                nama_dosen = nama_dosen,
                jumlah_sks = jumlah_sks,
                deskripsi_matkul = deskripsi_matkul,
                semester = semester,
                ruang_kelas = ruang_kelas

            )

            matkul.save()

            response['save_status'] = 'Mata kuliah berhasil ditambahkan'
        else:
            response['save_status'] = 'Mata kuliah gagal ditambahkan'
    else:
        response['save_status'] = 'Something went wrong'

    return render (request, 'save.html', response)

def ViewMatkul(request):
    list_matkul = MataKuliah.objects.all().values()
    response = {'list_matkul' : list_matkul}
    return render (request, 'viewmatkul.html', response)

def ClearMatkul(request, id_matkul = None):
    list_matkul = MataKuliah.objects.all().values()
    MataKuliah.objects.get(id=id_matkul).delete()
    response = {'list_matkul':list_matkul}
    return HttpResponseRedirect('/story4/viewmatkul/')

def DetailMatkul(request, id_matkul = None):
    matkul = MataKuliah.objects.get(id=id_matkul)
    response = {'matkul':matkul}
    return render (request, 'detailmatkul.html', response)

def AddKegiatan(request):
    response = {'create_kegiatan' : CreateKegiatan}
    return render (request, 'addkegiatan.html', response)

def SaveKegiatan(request):
    response = {'save_status':''}
    if (request.method == 'POST'):
        form = CreateKegiatan(request.POST)
        if (form.is_valid()):
            nama_kegiatan = form.cleaned_data['nama_kegiatan']

            kegiatan = Kegiatan(
                nama_kegiatan = nama_kegiatan
            )

            kegiatan.save()
        
            response['save_status'] = 'Kegiatan berhasil ditambahkan'
        else:
            response['save_status'] = 'Kegiatan gagal ditambahkan'
    else:
        response['save_status'] = 'Something went wrong'

    return render (request, 'save.html', response)

def ViewKegiatan(request):
    kegiatan_temp = []
    list_kegiatan = Kegiatan.objects.all().values()
    for kegiatan in list_kegiatan:
        list_peserta = Peserta.objects.filter(kegiatan=Kegiatan.objects.get(id=kegiatan['id']))
        kegiatan_temp.append(KegiatanTemp(kegiatan['id'], kegiatan['nama_kegiatan'], list_peserta))
    response = {'list_kegiatan' : kegiatan_temp}
    return render (request, 'viewkegiatan.html', response)

def AddPeserta(request, id_kegiatan = None):
    response = {'create_peserta' : CreatePeserta, 'id_kegiatan' : id_kegiatan}
    return render (request, 'addpeserta.html', response)

def SavePeserta(request, id_kegiatan = None):
    response = {'save_status':''}
    if (request.method == 'POST'):
        form = CreatePeserta(request.POST)
        if (form.is_valid()):
            nama_peserta = form.cleaned_data['nama_peserta']
            peserta = Peserta(
                nama_peserta = nama_peserta,
                kegiatan = Kegiatan.objects.get(id=id_kegiatan)
            )

            peserta.save()
        
            response['save_status'] = 'Peserta berhasil ditambahkan'
        else:
            response['save_status'] = 'Peserta gagal ditambahkan'
    else:
        response['save_status'] = 'Something went wrong'

    return render (request, 'save.html', response)

def ClearKegiatan(request):
    list_kegiatan = Kegiatan.objects.all().values()
    Kegiatan.objects.all().delete()
    response = {'list_kegiatan':list_kegiatan}
    return HttpResponseRedirect('/story4/viewkegiatan/')

def accordion(request):
    return render (request, 'accordion.html')

def search(request):
    response = {"SearchForm" : SearchForm}
    return render(request, "searchbox.html", response)
