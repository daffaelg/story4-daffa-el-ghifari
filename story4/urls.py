from django.urls import include, path

from .views import *

app_name = 'story4'

urlpatterns = [
    path('', profile, name='profile'),
    path('desc/', desc, name='desc'),
    path('story1/', story1, name='story1'),
    path('addmatkul/', AddMatkul, name = 'addmatkul'),
    path('addmatkul/savematkul/', SaveMatkul, name = 'savematkul'),
    path('viewmatkul/', ViewMatkul, name = 'viewmatkul'),
    path('viewmatkul/<int:id_matkul>/', ClearMatkul, name = 'clearmatkul'),
    path('viewmatkul/detail/<int:id_matkul>/', DetailMatkul, name = 'detailmatkul'),
    path('addkegiatan/', AddKegiatan, name = 'addkegiatan'),
    path('addkegiatan/savekegiatan/', SaveKegiatan, name = 'savekegiatan'),
    path('viewkegiatan/', ViewKegiatan, name = 'viewkegiatan'),
    path('viewkegiatan/addpeserta/<int:id_kegiatan>/', AddPeserta, name = 'addpeserta'),
    path('viewkegiatan/addpeserta/<int:id_kegiatan>/savepeserta/', SavePeserta, name = 'savepeserta'),
    path('viewkegiatan/clear/', ClearKegiatan, name = 'clearkegiatan'),
    path('accordion/', accordion, name = 'accordion'),
    path('search/', search, name = 'search')
    ]
