from django.db import models

class MataKuliah(models.Model):
    nama_matkul = models.CharField(max_length=50)
    nama_dosen = models.CharField(max_length=50)
    jumlah_sks = models.CharField(max_length=50)
    deskripsi_matkul = models.CharField(max_length=50)
    semester = models.CharField(max_length=50)
    ruang_kelas = models.CharField(max_length=50)

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=50)

class Peserta(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    nama_peserta = models.CharField(max_length=50)

class KegiatanTemp:
    def __init__(self, id, nama, list_peserta):
        self.id = id
        self.nama = nama
        self.list_peserta = list_peserta