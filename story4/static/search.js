$(document).ready(function(){
	var name = "hamlet";
	search(name);
})

$(document).on("click", "#button-search", function(event){
    event.preventDefault();
    search(null);
})


function search(name){
	var buku = name;
	if(buku == null){
		buku = $("#search-name").val();
	}

	$.ajax({
		url:'https://www.googleapis.com/books/v1/volumes?q=' + buku,
		data:{'query': buku},
		dataType: 'json',

		success:function(result){
			if ($.trim($("#books").html()).length != 0 ) {
                $("#books").empty();
            }

			for (i = 0; i < result.items.length; i++) {
                var judul = result.items[i].volumeInfo.title;
                var desc = result.items[i].volumeInfo.description;
                var penulis = result.items[i].volumeInfo.authors;
                var img = result.items[i].volumeInfo.imageLinks.smallThumbnail;

                if(desc == undefined){
                    desc = "Deskripsi tidak ditemukan";
                }
                if(penulis == undefined || penulis.length == 0){
                    var penulis = "Penulis tidak ditemukan";
                }
                
                var penulis_str = penulis.join(", ");

                $("#books").append(
                    "<tr>" 
                        + "<td>" + judul + "</td>" 
                        + "<td> <img src=" + img + "></td>" 
                        + "<td>" + desc + "</td>" 
                        + "<td>" + penulis_str + "</td>" 
                    + "</tr>");
            }
		}
	})
}